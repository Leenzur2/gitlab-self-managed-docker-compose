version: "3.5"

services:
  gitlab:
    image: gitlab/gitlab-ce:15.2.2-ce.0
    restart: always
    ports:
      - "2222:22"
    environment:
      TZ: "YOUR_TIMEZONE"
      GITLAB_OMNIBUS_CONFIG: |
        external_url 'https://YOUR_GITLAB_DOMAIN'
        nginx['redirect_http_to_https'] = false
        gitlab_rails['gitlab_ssh_host'] = "YOUR_GITLAB_DOMAIN"
        gitlab_rails['time_zone'] = 'YOUR_TIMEZONE'
        gitlab_rails['gitlab_shell_ssh_port'] = 2222

        # # Authentication from "GitLab.com" and ease imports
        # # Uncomment lines below if you want to use gitlab.com as an SSO auth provider
        # gitlab_rails['omniauth_enabled'] = true
        # gitlab_rails['omniauth_providers'] = [
        #   {
        #     name: "gitlab",
        #     # label: "Provider name", # optional label for login button, defaults to "GitLab.com"
        #     app_id: "YOUR_GITLABDOTCOM_APP_ID",
        #     app_secret: "YOUR_GITLABDOTCOM_SECRET",
        #     args: { scope: "read_user" } # optional: defaults to the scopes of the application
        #   }
        # ]
        # gitlab_rails['omniauth_allow_single_sign_on'] = ['gitlab']
        # gitlab_rails['omniauth_sync_profile_from_provider'] = ['gitlab']
        # gitlab_rails['omniauth_block_auto_created_users'] = false
        # gitlab_rails['omniauth_sync_profile_attributes'] = ['email']
        # gitlab_rails['omniauth_auto_sign_in_with_provider'] = 'gitlab'



        # Nginx params to redirect from traefik
        nginx['listen_https'] = false
        nginx['listen_port'] = 80
        nginx['proxy_set_headers'] = {
          "X-Forwarded-Proto" => "https",
          "X-Forwarded-Ssl" => "on"
        }
        letsencrypt['enable'] = false

        # SMTP
        gitlab_rails['gitlab_email_from'] = 'gitlab-no-reply@your.domain.com'
        gitlab_rails['gitlab_email_display_name'] = 'GitLab Administrator'
        gitlab_rails['gitlab_email_reply_to'] = 'support@your.domain.com'
        gitlab_rails['smtp_enable'] = true
        gitlab_rails['smtp_address'] = "YOUR_GITLAB_SMTP_ADDR"
        gitlab_rails['smtp_port'] = "YOUR_GITLAB_SMTP_PORT"
        gitlab_rails['smtp_user_name'] = "YOUR_GITLAB_SMTP_USER"
        gitlab_rails['smtp_password'] = "YOUR_GITLAB_SMTP_PASSWORD"
        gitlab_rails['smtp_domain'] = "YOUR_GITLAB_SMTP_DOMAIN"
        gitlab_rails['smtp_authentication'] = "login"
        gitlab_rails['smtp_enable_starttls_auto'] = true
        gitlab_rails['smtp_tls'] = false
        gitlab_rails['smtp_openssl_verify_mode'] = 'peer'

        # Logging
        logging['logrotate_frequency'] = "weekly"
        logging['logrotate_rotate'] = 52
        logging['logrotate_compress'] = "compress"
        logging['logrotate_method'] = "copytruncate"
        logging['logrotate_delaycompress'] = "delaycompress"

        # GITLAB DOCKER IMAGE REGISTRY: so that we can use our docker image registry with gitlab
        registry_external_url 'https://YOUR_GITLAB_REGISTRY_DOMAIN'
        gitlab_rails['registry_enabled'] = true
        gitlab_rails['registry_api_url'] = 'https://YOUR_GITLAB_REGISTRY_DOMAIN'
        registry['enable'] = true
        registry_nginx['enable'] = false
        registry['registry_http_addr'] = "0.0.0.0:5000"
    volumes:
      - ./docker/gitlab/config:/etc/gitlab
      - ./docker/gitlab/logs:/var/log/gitlab
      - ./docker/gitlab/data:/var/opt/gitlab
    labels:
      # gitlab
      - traefik.http.services.gitlab.loadbalancer.server.port=80
      - traefik.http.routers.gitlab.rule=Host(`YOUR_GITLAB_DOMAIN`)
      - traefik.http.routers.gitlab.service=gitlab
      - traefik.http.routers.gitlab.tls=true
      - traefik.http.routers.gitlab.tls.certresolver=le
      - traefik.http.routers.gitlab.entrypoints=https
      - traefik.http.routers.gitlab.middlewares=gzip-compress

      # gitlab-registry
      - traefik.http.services.gitlab-registry.loadbalancer.server.port=5000
      - traefik.http.routers.gitlab-registry.rule=Host(`YOUR_GITLAB_REGISTRY_DOMAIN`)
      - traefik.http.routers.gitlab-registry.service=gitlab-registry
      - traefik.http.routers.gitlab-registry.tls=true
      - traefik.http.routers.gitlab-registry.tls.certresolver=le
      - traefik.http.routers.gitlab-registry.entrypoints=https

      # # Note: all dollar signs in the hash need to be doubled for escaping.
      # # To create user:password pair, it's possible to use this command:
      # # echo $(htpasswd -nb user password) | sed -e s/\\$/\\$\\$/g
      # - 'traefik.http.middlewares.auth.basicauth.users=username:$$pass$$word$$to$$generate$$and$$replace'
    shm_size: '256m'

  gitlab-runner:
    image: gitlab/gitlab-runner:v15.2.2
    restart: always
    volumes:
      - "./docker/gitlab-runner/config/:/etc/gitlab-runner:Z"
      - "/var/run/docker.sock:/var/run/docker.sock"
      - "/builds:/builds"
    labels:
      - "traefik.enable=false"
    depends_on:
      - gitlab
    shm_size: '256m'

  traefik:
    image: "traefik:v2.2"
    restart: always
    command:
    #   - "--log.level=DEBUG"
      - --entrypoints.http.address=:80
      - --entrypoints.https.address=:443
      - --providers.docker=true
      - --certificatesresolvers.le.acme.email=informatique@your.domain.com
      - --certificatesresolvers.le.acme.storage=/acme/acme.json
      - --certificatesresolvers.le.acme.tlschallenge=true
      # - --certificatesresolvers.le.acme.caserver=https://acme-staging-v02.api.letsencrypt.org/directory
    ports:
      - "80:80"
      - "443:443"
      # - "5000:5000"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock:ro"
      - "./docker/traefik/acme:/acme"
    labels:
      - "traefik.enable=true"
      # middleware to redirect www to non-www
      - "traefik.http.middlewares.www-redirect.redirectregex.regex=^https://www.YOUR_GITLAB_DOMAIN(.*)"
      - "traefik.http.middlewares.www-redirect.redirectregex.replacement=https://YOUR_GITLAB_DOMAIN$${1}"
      - "traefik.http.middlewares.www-redirect.redirectregex.permanent=true"
      # middleware and router to redirect http to https
      - "traefik.http.routers.https-redirect.entryPoints=http"
      - "traefik.http.routers.https-redirect.rule=HostRegexp(`{any:.*}`)"
      - "traefik.http.routers.https-redirect.middlewares=https-redirect"
      - "traefik.http.middlewares.https-redirect.redirectscheme.scheme=https"
      # gzip compression
      - "traefik.http.middlewares.gzip-compress.compress=true"
