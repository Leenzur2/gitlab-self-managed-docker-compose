# gitlab-self-managed-docker-compose


git clone the repository

edit the docker-compose.yml
you can find all variables to replace with a research on 'YOUR_*' in the file

note : the docker-compose.yml file is edited work in with an SSL certificate, this will be generated automatically with letsencrypt. You can contribute or ask me a "without SSL" version.

docker-compose up -d

wait for gitlab container to be healthy
watch 'docker ps'

visit your domain
dns can take several times to propagate you domain
